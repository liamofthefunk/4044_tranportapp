from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
import mysql.connector



app = Flask (__name__)

mydatabase = mysql.connector.connect(
	host = 'sql2.freemysqlhosting.net', user = 'sql2317458',
	passwd = 'hM4!qL1*', database = 'sql2317458')

mycursor = mydatabase.cursor()

# CODE FOR PAGES USED HER https://www.spiderposts.com/2019/07/04/flask-sqlalchemy-tutorial-login-system-with-python/

#login page SQL needs testing once SQL added to login page !!!STILL NNEEDS WORK!!!
@app.route("/",methods=["GET","POST"])
def login():
	if request.method=="POST":
		user_name=request.form.get("name")
		password=request.form.get("password")

		usernamedata=db.execute("SELECT user_name FROM users WHERE user_name=:user_name",{"user_name":user_name}).fetchone()
		passworddata=db.execute("SELECT password FROM users WHERE user_name=:user_name",{"user_name":user_name}).fetchone()

		if usernamedata is None:
			flash("No username","danger")
			return render_template('login.html')
		else:
			for passwor_data in passworddata:
				if sha256_crypt.verify(password,passwor_data):
					session["log"]=True
					flash("You are now logged in!!","success")
					return redirect(url_for('hello')) #to be edited from here do redict to dashboard
				else:
					flash("incorrect password","danger")
					return render_template('/templates/login.html')

	return render_template('login.html')


#Register page code !!!!!STILL NEEDS TO BE LOOKED AT!!!!
@app.route("/register",methods=["GET","POST"])
def register():
	if request.method=="POST":
		name=request.form.get("name")
		username=request.form.get("user_name")
		password=request.form.get("password")
		confirm=request.form.get("confirm")
		secure_password=sha256_crypt.encrypt(str(password))

		usernamedata=db.execute("SELECT user_name FROM users WHERE user_name=:user_name",{"user_name":user_name}).fetchone()
		#usernamedata=str(usernamedata)
		if usernamedata==None:
			if password==confirm:
				db.execute("INSERT INTO users(name,user_name,password) VALUES(:name,:username,:password)",
					{"name":name,"user_name":user_name,"password":secure_password})
				db.commit()
				flash("You are registered and can now login","success")
				return redirect(url_for('login'))
			else:
				flash("password does not match","danger")
				return render_template('register.html')
		else:
			flash("user already existed, please login or contact admin","danger")
			return redirect(url_for('login'))

	return render_template('register.html')


@app.route("/dashboard.html")
def dash():
	mycursor.execute('SELECT * FROM jobs WHERE title like '%Searchstring%') ')
	result = mycursor.fetchall()
	return render_template('dashboard.html',page = 'Search', output_data=result)

if __name__=='__main__':
	app.run(debug=True)
