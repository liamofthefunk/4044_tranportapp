# Welcome to our transport app

This project is designed to run a flask enviroment that will allow the user to run
the prototype version of our transportation dashboard.

GETTING STARTED:

These instructions will get you a copy of the project up and running on your local
machine for development and testing purposes.

REQUIREMENTS:

cd into floder that holds the beta.py file.

Flask ( $ pip install Flask ) is required to run the project.

INSTALLATION:

You can create a virtual environment and install the required packages with the following commands:

$ virtualenv venv

$ . venv/bin/activate

(venv) $ pip install -r requirements.txt

RUNNING THE PROJECT:

With the virtual environment activated and making sure you have cd into right folder:

(venv) $ python beta.py

Once the flask app is running you can open your browser and go to 127.0.0.1:5000/register and the project should
appear.

#We have also included an SQL file containing the db tables used but as we have hosted them online you 
#will not have to worry about them when you attempt to run the app.





# Test flask-mysql app

This is a skeleton Flask-MySQL application.

## Installation

First rename `config.py.example` -> `config.py`

### With Docker

To run the app with Docker you need docker-compose version 3,

+ Open `config.py` and edit `DockerDevConfig` with your MySQL credentials
+ Save and close the file
+ Now do,

		docker-compose up -d

+ To stop the containers,

		docker-compose stop

### Without Docker

To run it from a local Python environment and local MySQL database install, 

1. Install dependencies in your python environment

		pip3 install -r requirements.txt

2. Ensure you have a MySQL database install running on port 3306
3. Open `config.py` and edit `DevConfig` with your MySQL credentials
4. Open `__init__.py` and change `DockerDevConfig` to `DevConfig`
5. Then do,

		python run.py

Other configuration options are possible if you edit `config.py` with a new configuration.

**Do not commit/push your `config.py` file.**

# Google drive link
https://drive.google.com/open?id=1vdqoVXsmRu8pgANyDkAKX1tFEuhvvDGt

# Branch names

	HTML Code:

		git checkout html

	database code:

		git checkout database

	Return to master branch 
		
		git checkout master
