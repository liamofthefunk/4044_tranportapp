from flask import Flask, render_template, url_for,request,session,logging,redirect,flash
from flask_sqlalchemy import SQLAlchemy
#import mysql.connector
import os
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session,sessionmaker

from passlib.hash import sha256_crypt
engine=create_engine("mysql+pymysql://sql2317458:hM4!qL1*@sql2.freemysqlhosting.net/sql2317458")
					#mysql+pymysql://username:password@localhost/databasename

db=scoped_session(sessionmaker(bind=engine))



app = Flask (__name__)
#app.static_folder = 'static'

app.secret_key = 'the random string'


# CODE FOR PAGES USED HER https://www.spiderposts.com/2019/07/04/flask-sqlalchemy-tutorial-login-system-with-python/

#login page SQL needs testing once SQL added to login page !!!STILL NNEEDS WORK!!!
@app.route("/login",methods=["GET","POST"])
def login():
	if request.method=="POST":
		username=request.form.get("username")
		password=request.form.get("password")

		usernamedata=db.execute("SELECT username FROM users WHERE username=:username",{"username":username}).fetchone()
		passworddata=db.execute("SELECT password FROM users WHERE password=:username",{"username":username}).fetchone()
		hash = sha256_crypt.hash("password")
		#if usernamedata is None:
		#	flash("No username","danger")
		#	return render_template('login.html')
		#else:
			#for passwor_data in passworddata:


		if sha256_crypt.verify("password",hash):
					session["log"]=True
					flash("You are now logged in!!","success")
					return redirect(url_for('dashboard')) #to be edited from here do redict to dashboard
		else:
					flash("incorrect password","danger")
					return render_template('login.html')

	return render_template('login.html')


#Register page code !!!!!STILL NEEDS TO BE LOOKED AT!!!!
@app.route("/register",methods=["GET","POST"])
def register():
	if request.method=="POST":
		#return render_template("register.html")
		email=request.form.get("email")
		username=request.form.get("username")
		password=request.form.get("password")
		confirm=request.form.get("confirm")
		secure_password=sha256_crypt.hash("password")

		if password==confirm:	
				db.execute("INSERT INTO users(email,username,password) VALUES(:email,:username,:password)",
					{"email":email,"username":username,"password":secure_password})
				db.commit()

				flash("You are registered and can now login","success")
				return redirect(url_for('login'))
		#elif:
		#		flash("password does not match","danger")
		#		return render_template('register.html')
		#else:
		#	flash("user already existed, please login or contact admin","danger")
		#	return redirect(url_for('login'))

	return render_template('register.html')


@app.route("/dashboard", methods=["GET","POST"])
def dashboard():
	result = db.execute('SELECT * FROM transpo_data')
	data = result.fetchall()
	result.close()

	#search formatting https://code-maven.com/slides/python-programming/sqlalchemy-engine-select-fetchall

	return render_template('index.html', page = 'Home') #output_data = data)

@app.route("/survey")
def survey():
	return render_template('survey.html')
		
@app.route("/poll", methods=["GET","POST"])
def poll():
		if request.method=="POST":
			bike=request.form.get("bike")
			car=request.form.get("car")
			bus=request.form.get("bus")
			walk=request.form.get("walk")
			train=request.form.get("train")
	
			db.execute("INSERT INTO transpo_data (bike,car,bus,walk,train) VALUES(:bike,:car,:bus,:walk,:train)",
					{"bike":bike,"car":car,"bus":bus,"walk":walk,"train":train})
			db.commit()

			return redirect(url_for('dashboard'))



if __name__=='__main__':
	app.run(debug=True)
