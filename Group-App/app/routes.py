from flask import render_template
from app import app, mysql

# ----------------------
# APP ROUTES
# ----------------------

@app.route('/')
def index():
    """
        Landing page for application
    """
    return render_template('home.html', page="Home")

@app.route('/test-db')
def test_db():
    """
        Route to test database connection
    """
    # make a database cursor
    cursor = mysql.get_db().cursor()
    # execute a query
    result = cursor.execute("SELECT 1")
    # close cursor
    cursor.close()
    if result == 1:
        return "Database connection succeeded"
    else:
        return "Database connection failed"
